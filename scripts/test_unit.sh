#!/usr/bin/env bash

set -e

echo "RUNNING UNIT TESTS..."
echo "---------------------"
echo -e "\n\n"
bundle exec ruby ./test/test_helper.rb
echo -e "\n\n"
