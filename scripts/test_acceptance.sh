#!/usr/bin/env bash

set -e

echo "RUNNING ACCEPTANCE TESTS..."
echo "---------------------------"
echo -e "\n\n"
bundle exec cucumber
echo -e "\n\n"
