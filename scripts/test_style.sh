#!/usr/bin/env bash

set -e

echo "RUNNING STYLE TESTS....."
echo "------------------------"
echo -e "\n\n"
bundle exec rubocop -c .rubocop.yaml
echo -e "\n\n"
