#!/usr/bin/env bash

set -e

echo "RUNNING METRIC TESTS....."
echo "------------------------"
echo -e "\n\n"
bundle exec flog -g lib -g test
echo -e "\n\n"
