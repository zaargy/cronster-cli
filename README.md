# Cronster-CLI

* Repository: https://bitbucket.org/zaargy/cronster-cli
* CircleCI build: https://circleci.com/bb/zaargy/cronster-cli

## How to run tests:

`make up`

`make test`

## Dependencies:

* Bash-compatible shell
* Make
* Docker

## Methodology/Principles used:

* Outside-in ATDD/TDD
* SOLID principles

## Todos:

* Error checking could be improved and tested more thoroughly (stopped due to time-constraints)
* The parser could be simplied and made more readable (stopped due to time-constraints)

## Cool stuff:

* Linting with Rubocop
* CircleCI build
* Easy to get going since uses Docker

## Note:

Since * characters and such are interpreted by the shell I have assumed that the expression
is specified in quotes:

`bin/cronster-cli "0 * * * 1-5 /usr/bin/find"`

