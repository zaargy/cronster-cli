require 'cronster/exceptions'

module Cronster
  # :nodoc:
  class FieldParser
    def initialize(lower_bound, upper_bound)
      @lower_bound = lower_bound
      @upper_bound = upper_bound
    end

    def parse(field_expression)
      field_expression.to_s.split(/,/).map do |part|
        parse_part(part)
      end.flatten.sort.uniq
    rescue StandardError
      raise Cronster::InvalidExpressionException
    end

    private

    def parse_part(part)
      part = part.strip

      if part.include?('-')
        parse_range(part)
      elsif part.include?('/')
        parse_increment(part)
      elsif part.include?('*')
        parse_star
      else
        parse_literal(part)
      end
    end

    def parse_range(part)
      from, to = part.split(/-/).map do |x|
        parse_literal(x)
      end

      (from..to).to_a
    end

    def parse_increment(part)
      from, increment = part.tr('*', @lower_bound.to_s).split(%r{/}).map do |x|
        parse_literal(x)
      end

      (from..@upper_bound).step(increment).to_a
    end

    def parse_star
      (@lower_bound..@upper_bound).to_a
    end

    def parse_literal(part)
      Integer(part.strip)
    end
  end
end
