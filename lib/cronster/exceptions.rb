module Cronster
  class GenericInvalidException < StandardError; end
  class InvalidExpressionException < StandardError; end
  class NoCommandSpecifiedException < StandardError; end
  class NotEnoughTimePartsException < StandardError; end
end
