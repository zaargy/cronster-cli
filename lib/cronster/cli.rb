require 'cronster/parser'
require 'cronster/presenter'

module Cronster
  # :nodoc:
  class CLI
    def initialize(parser = Cronster::Parser.new,
                   presenter = Cronster::Presenter.new)
      @parser = parser
      @presenter = presenter
    end

    def evaluate(expression)
      @presenter.present(@parser.parse(expression))
    rescue Cronster::NotEnoughTimePartsException
      'Invalid cron expression'
    rescue InvalidExpressionException
      'Invalid cron expression'
    rescue Cronster::GenericInvalidException
      'Invalid cron expression'
    end
  end
end
