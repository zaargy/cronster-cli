require 'cronster/command_field_parser'
require 'cronster/exceptions'
require 'cronster/field_parser'

module Cronster
  # :nodoc:
  class Parser
    def initialize(minute_parser = FieldParser.new(0, 59),
                   hour_parser = FieldParser.new(0, 23),
                   day_of_month_parser = FieldParser.new(1, 31),
                   month_parser = FieldParser.new(1, 12),
                   day_of_week_parser = FieldParser.new(0, 6),
                   command_parser = CommandFieldParser.new)
      @minute_parser = minute_parser
      @hour_parser = hour_parser
      @day_of_month_parser = day_of_month_parser
      @month_parser = month_parser
      @day_of_week_parser = day_of_week_parser
      @command_parser = command_parser
    end

    def parse(expression)
      minute, hour, day_of_month,
      month, day_of_week, command = expression.split(/\s+/)

      if minute.nil? || hour.nil? || day_of_month.nil? || month.nil? ||
         day_of_week.nil? || command.nil?
        raise Cronster::NotEnoughTimePartsException
      end

      {
        minute: @minute_parser.parse(minute),
        hour: @hour_parser.parse(hour),
        day_of_month: @day_of_month_parser.parse(day_of_month),
        month: @month_parser.parse(month),
        day_of_week: @day_of_week_parser.parse(day_of_week),
        command: @command_parser.parse(command)
      }
    end
  end
end
