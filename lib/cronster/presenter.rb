module Cronster
  # :nodoc:
  class Presenter
    def present(expression)
      output = "minute         #{expression[:minute].join(' ')}\n"
      output << "hour           #{expression[:hour].join(' ')}\n"
      output << "day of month   #{expression[:day_of_month].join(' ')}\n"
      output << "month          #{expression[:month].join(' ')}\n"
      output << "day of week    #{expression[:day_of_week].join(' ')}\n"
      output << "command        #{expression[:command]}"

      output
    end
  end
end
