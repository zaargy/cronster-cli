docker_sh   = docker exec -w /app -it shell
docker_sh_c = $(docker_sh) sh -c

build:
	@docker-compose down
	@docker-compose up -d

ci_install:
	bundle install

install:
	@$(docker_sh_c) 'bundle install'

shell:
	@$(docker_sh) sh

test: test_style test_unit test_acceptance test_metrics

up: build install

ci_test:
	$(shell pwd)/scripts/test_style.sh
	$(shell pwd)/scripts/test_unit.sh
	$(shell pwd)/scripts/test_acceptance.sh
	$(shell pwd)/scripts/test_metrics.sh

test_style:
	@$(docker_sh_c) './scripts/test_style.sh'

test_unit:
	@$(docker_sh_c) './scripts/test_unit.sh'

test_acceptance:
	@$(docker_sh_c) './scripts/test_acceptance.sh'

test_metrics:
	@$(docker_sh_c) './scripts/test_metrics.sh'

git_status:
	@$(docker_sh_c) './scripts/git_status.sh'

precommit: test git_status

ci: ci_install

.PHONY: build ci_install install shell test up ci_test test_style test_unit test_acceptance test_metrics precommit ci
