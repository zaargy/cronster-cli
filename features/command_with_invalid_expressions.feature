Feature: Run command with a mix of invalid cron expressions
  Scenario: Run invalid expression with not enough time parts
    When I run `cronster-cli "0 1,15 * 1-5 /usr/bin/find"`
    Then the output should contain:
	"""
Invalid cron expression
	"""

  Scenario: Run invalid expression with invalid characters in time part
    When I run `cronster-cli "0 A * * 1-5 /usr/bin/find"`
    Then the output should contain:
	"""
Invalid cron expression
	"""
