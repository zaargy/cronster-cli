Feature: Run command with a mix of valid cron expressions
  Scenario: Run valid mixture expression
    When I run `cronster-cli "*/15 0 1,15 * 1-5 /usr/bin/find"`
    Then the output should contain:
	"""
minute         0 15 30 45
hour           0
day of month   1 15
month          1 2 3 4 5 6 7 8 9 10 11 12
day of week    1 2 3 4 5
command        /usr/bin/find
	"""

  Scenario: Run another valid expression
    When I run `cronster-cli "* * * * * /usr/bin/find"`
    Then the output should contain:
	"""
minute         0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59
hour           0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23
day of month   1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31
month          1 2 3 4 5 6 7 8 9 10 11 12
day of week    0 1 2 3 4 5 6
command        /usr/bin/find
	"""

  Scenario: Run yet another valid expression
    When I run `cronster-cli "1,2,3 * * * * /usr/bin/find"`
    Then the output should contain:
	"""
minute         1 2 3
hour           0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23
day of month   1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31
month          1 2 3 4 5 6 7 8 9 10 11 12
day of week    0 1 2 3 4 5 6
command        /usr/bin/find
	"""

  Scenario: Run another valid yet slightly confusing expression
    When I run `cronster-cli "*/5 2,*/7 5,*/1 * * /usr/bin/find"`
    Then the output should contain:
	"""
minute         0 5 10 15 20 25 30 35 40 45 50 55
hour           0 2 7 14 21
day of month   1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31
month          1 2 3 4 5 6 7 8 9 10 11 12
day of week    0 1 2 3 4 5 6
command        /usr/bin/find
	"""


