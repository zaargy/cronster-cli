require 'simplecov'

SimpleCov.start

$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), '..', 'lib'))

require 'minitest/autorun'

if $PROGRAM_NAME == __FILE__
  Dir.glob('./test/**/test_*.rb').each do |f|
    require f
  end
end
