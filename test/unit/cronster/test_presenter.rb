require 'cronster/presenter'

module Cronster
  # :nodoc:
  class TestPresenter < Minitest::Test
    def test_it_presents_in_table_format
      presenter = Cronster::Presenter.new

      expected_output = "minute         0 15 30 45\n"
      expected_output << "hour           0\n"
      expected_output << "day of month   1 15\n"
      expected_output << "month          1 2 3 4 5 6 7 8 9 10 11 12\n"
      expected_output << "day of week    1 2 3 4 5\n"
      expected_output << 'command        /usr/bin/find'

      cron_hash = {
        minute: [0, 15, 30, 45],
        hour: [0],
        day_of_month: [1, 15],
        month: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
        day_of_week: [1, 2, 3, 4, 5],
        command: '/usr/bin/find'
      }

      assert_equal expected_output, presenter.present(cron_hash)
    end
  end
end
