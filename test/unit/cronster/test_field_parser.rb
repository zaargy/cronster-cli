require 'cronster/field_parser'

module Cronster
  # :nodoc:
  class TestFieldParser < Minitest::Test
    def setup
      @field_parser = FieldParser.new(0, 59)
    end

    def test_it_parses_star
      assert_equal (0..59).to_a, @field_parser.parse('*')
    end

    def test_it_parses_valid_literal
      assert_equal [15], @field_parser.parse('15')
    end

    def test_it_parses_valid_set_of_literals
      assert_equal [15, 30, 32, 45], @field_parser.parse('15,30,32,45')
    end

    def test_it_parses_valid_range
      assert_equal (0..59).to_a, @field_parser.parse('0-59')
      assert_equal (15..30).to_a, @field_parser.parse('15-30')
    end

    def test_it_parses_a_valid_increment
      assert_equal [0, 15, 30, 45], @field_parser.parse('*/15')
      assert_equal [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55],
                   @field_parser.parse('*/5')
    end

    def test_it_parses_a_valid_mixture
      assert_equal [0, 1, 15, 30, 31, 32, 33, 45],
                   @field_parser.parse('1,30,31-33,*/15')
      assert_equal [0, 1, 2, 3, 4, 5, 10, 15, 20,
                    24, 25, 26, 28, 30, 35, 40, 45, 50, 55],
                   @field_parser.parse('1-2,3-4,*/5,24,25,26,28')
    end

    def test_it_raises_an_exception_for_invalid_expression
      assert_raises Cronster::InvalidExpressionException do
        @field_parser.parse('A * * * * /usr/bin/find')
        @field_parser.parse('* - * * * /usr/bin/find')
        @field_parser.parse('* , A * * /usr/bin/find')
        @field_parser.parse('1 * * A * /usr/bin/find')
        @field_parser.parse('* * * * A /usr/bin/find')
      end
    end
  end
end
