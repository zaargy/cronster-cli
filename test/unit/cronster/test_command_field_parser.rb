require 'cronster/command_field_parser'

module Cronster
  # :nodoc:
  class TestCommandFieldParser < Minitest::Test
    def setup
      @command_field_parser = CommandFieldParser.new
    end

    def test_it_parses_command
      assert_equal '/usr/bin/find', @command_field_parser.parse('/usr/bin/find')
    end
  end
end
