require 'cronster/parser'

module Cronster
  class FakeFieldParser
    def parse(expression)
      expression
    end
  end

  class TestParser < Minitest::Test
    def setup
      minute_parser = FakeFieldParser.new
      hour_parser = FakeFieldParser.new
      day_of_month_parser = FakeFieldParser.new
      month_parser = FakeFieldParser.new
      day_of_week_parser = FakeFieldParser.new
      command_parser = FakeFieldParser.new

      @parser = Cronster::Parser.new(minute_parser, hour_parser,
                                     day_of_month_parser, month_parser,
                                     day_of_week_parser, command_parser)
    end

    def test_it_parses_a_valid_cron_expression_into_six_parts
      assert_equal 6, @parser.parse('* * * * * /usr/bin/find').keys.count
    end

    def test_it_raises_an_exception_for_invalid_cron_exp
      assert_raises Cronster::NotEnoughTimePartsException do
        @parser.parse('* * * * /usr/bin/find')
      end
    end
  end
end
